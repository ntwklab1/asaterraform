terraform {
  required_providers {
    ciscoasa = {
      source  = "CiscoDevNet/ciscoasa"
      version = "1.3.0"
    }
  }
}
provider "ciscoasa" {
  api_url       = "https://172.16.1.250"
  username      = var.ASA_USER
  password      = var.ASA_PASSWORD
  ssl_no_verify = true
}

variable "ASA_USER" {
  type = string
}

variable "ASA_PASSWORD" {
  type = string
}



resource "ciscoasa_interface_physical" "gig_outside" {
  name           = "OUTSIDE"
  hardware_id    = "GigabitEthernet0/0"
  interface_desc = "OUTSIDE_TERRAFORM_CREATED"
  ip_address {
    dhcp {
      dhcp_option_using_mac = false
      dhcp_broadcast        = true
      dhcp_client {
        set_default_route = true
        metric            = 1
        primary_track_id  = 6
        tracking_enabled  = false
      }
    }
  }
  security_level = 0
  shutdown       = false
}

resource "ciscoasa_interface_physical" "gig1_inside" {
  name           = "INSIDE"
  hardware_id    = "GigabitEthernet0/1"
  interface_desc = "INSIDE_TERRAFORM_CREATED"
  ip_address {
    static {
      ip       = "192.168.1.1"
      net_mask = "255.255.255.0"
    }
  }
  security_level = 100
  shutdown       = false
}


resource "ciscoasa_interface_physical" "gig2_dmz" {
  name           = "DMZ"
  hardware_id    = "GigabitEthernet0/2"
  interface_desc = "DMZ_TERRAFORM_CREATED"
  ip_address {
    static {
      ip       = "192.168.2.1"
      net_mask = "255.255.255.0"
    }
  }
  security_level = 10
  shutdown       = false
}



resource "ciscoasa_static_route" "mgmt_static_route" {
  interface = "MGMT"
  network   = "10.0.0.0/8"
  gateway   = "172.16.1.254"
}




resource "ciscoasa_network_object" "dmz_server1" {
  name  = "DMZ_SERVER1"
  value = "192.168.2.5"
}

resource "ciscoasa_network_object" "dmz_server2" {
  name  = "DMZ_SERVER2"
  value = "192.168.2.6"
}

resource "ciscoasa_network_object" "dmz_server1_10_10_30_247" {
  name  = "dmz_server1_10_10_30_247"
  value = "10.10.30.247"
}

resource "ciscoasa_network_object_group" "all_dmz_servers" {
  name = "ALL_DMZ_SERVERS"

  members = [
    "${ciscoasa_network_object.dmz_server1.name}",
  ]
}


resource "ciscoasa_network_service" "tcp_80" {
  name  = "TCP_80"
  value = "tcp/80"
}

resource "ciscoasa_network_service" "tcp_443" {
  name  = "TCP_443"
  value = "tcp/443"
}

resource "ciscoasa_network_service_group" "web_ports" {
  name = "WEB_PORTS"

  members = [
    "${ciscoasa_network_service.tcp_80.name}",
    "${ciscoasa_network_service.tcp_443.name}",
  ]
}



resource "ciscoasa_access_in_rules" "all_dmz_servers_in_web_ports" {
  interface = "OUTSIDE"
  rule {
    source              = "0.0.0.0/0"
    destination         = ciscoasa_network_object_group.all_dmz_servers.name
    destination_service = ciscoasa_network_service_group.web_ports.name
  }
}








resource "ciscoasa_network_object_group" "all_inside_servers" {
  name = "ALL_INSIDE_SERVERS"

  members = [
    "192.168.1.5",
  ]
}


resource "ciscoasa_network_service_group" "ssh_port" {
  name = "SSH_PORT"

  members = [
    "tcp/22",
  ]
}

resource "ciscoasa_access_in_rules" "inside_dmz_ssh" {
  interface = "INSIDE"
  rule {
    source              = "192.168.1.5"
    destination         = ciscoasa_network_object_group.all_dmz_servers.name
    destination_service = ciscoasa_network_service_group.ssh_port.name
  }
}


resource "ciscoasa_nat" "auto_test" {
  section                   = "auto"
  description               = "static auto test"
  mode                      = "static"
  original_interface_name   = ciscoasa_interface_physical.gig2_dmz.name
  translated_interface_name = ciscoasa_interface_physical.gig_outside.name
  original_source_kind      = "objectRef#NetworkObj"
  original_source_value     = ciscoasa_network_object.dmz_server1.name
  translated_source_kind    = "objectRef#NetworkObj"
  translated_source_value   = ciscoasa_network_object.dmz_server1_10_10_30_247.name
}
